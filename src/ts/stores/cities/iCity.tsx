export interface ICity {
    name: string;
    timezone: string;
}
