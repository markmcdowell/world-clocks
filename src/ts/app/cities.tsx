import * as React from "react";
import { ICity } from "../stores/cities/iCity";
import { City } from "./city";

interface ICitiesProps {
    readonly cities: ICity[];
}

export class Cities extends React.PureComponent<ICitiesProps> {

    public render() {
        return (
            <>
                {
                    this.props.cities.map((city) => {
                        return (
                            <City
                                key={city.name}
                                name={city.name}
                                timezone={city.timezone} />
                        );
                    })
                }
            </>
        );
    }
}
